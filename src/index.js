import React from "react";
import {render} from "react-dom";
import App from "./components/App.js";
import 'bootstrap/dist/css/bootstrap.min.css'

const target = document.getElementById('root');

render(
    <App/>, target
);