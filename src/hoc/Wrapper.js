import React, {Component} from 'react';
import moment from "moment";

const Wrapper = (WrappedComponent) => {
    return class extends Component {
        state = {
            bitcoinData: []
        }

        componentDidMount() {
            this.fetchData('https://api.coindesk.com/v1/bpi/historical/close.json')
        }


        fetchData = async (req) => {
            try {
                this.setState({
                    isLoaded: false
                });

                const res = await fetch(req);
                const bitcoinData = await res.json();
                const sortedData = [];

                for (let date in bitcoinData.bpi) {
                    sortedData.push({
                        date: moment(date).format('MMM DD'),
                        priceDollar: bitcoinData.bpi[date].toFixed(2),
                        pricePound: (bitcoinData.bpi[date] * 0.75).toFixed(2) // numerical price
                    });
                }

                this.setState({
                    isLoaded: true,
                    bitcoinData: sortedData
                });
            } catch (err) {
                console.log(err.message)
            } finally {
                console.log('finally')
                const bitcoinData = this.props.mockData
                console.log(bitcoinData);
                const sortedData = [];


                bitcoinData.map((obj) => {
                    sortedData.push({
                            date: Object.values(obj)[0],
                            priceDollar: Object.values(obj)[1],
                            pricePound: Object.values(obj)[3]
                        }
                    )
                })

                this.setState({
                    isLoaded: true,
                    bitcoinData: sortedData
                });
            }
        }

        render() {
            return <WrappedComponent
                data={this.state.bitcoinData}
                {...this.props}/>
        }
    }
};


export default Wrapper;