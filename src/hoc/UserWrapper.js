const UsersWrapper = (WrappedComponent, type) => {
    class WithDataRenderingComponent extends Component {
        state = {
            page: null
        };

        static getDerivedStateFromProps(props, state) {
            const search = props.location.search;
            const params = search && new URLSearchParams(search);
            let currentPage = params && params.get('usersPage');
            if (!currentPage || currentPage === 'undefined' || currentPage === '' || currentPage === 0) {
                currentPage = props.defaultPage;
            }
            return {
                ...state,
                page: +currentPage
            };
        }

        render() {
            let {match: {params: {id: userUid}}} = this.props;
            if (!userUid) {
                userUid = ls.get('CURRENT_USER_UID');
            }
            const {page} = this.state;
            const restProps = {
                ...this.props,
                userUid,
                page
            };
            return <WrappedComponent {...restProps}/>;
        }
    }
}