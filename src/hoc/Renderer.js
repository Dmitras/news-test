import React from 'react';

const Renderer = props => props.children;

export default Renderer;