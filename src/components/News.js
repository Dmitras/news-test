import React, {Component} from 'react';
import NewsAPI from 'newsapi';
import moment from 'moment';
import {Badge, Spinner, Button} from 'reactstrap';
import styled from 'styled-components';
import {debounce} from 'lodash';
import PropTypes from 'prop-types';


const Sp = styled.p`
  font-size: 12px;
`;

const SLoading = styled(Spinner)`
  margin: 1em auto;
`;


class News extends Component {
    constructor(props) {
        super(props);
        this.fetchData = debounce(this.fetchData.bind(this), 500)
    }

    apiUri = 'https://newsapi.org/v2/everything?';
    state = {
        isLoaded: false,
        news: null,
        quantity: 10,
    };


    componentDidMount() {
        const req = this.generateRequest(this.props.query)
        this.fetchData(req)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const req = this.generateRequest(this.props.query)

        if (prevProps.query !== this.props.query) {
            this.setState({
                quantity: 10
            });
            this.fetchData(req)
        } else if (prevState.quantity !== this.state.quantity) {
            this.fetchData(req)
        }

        console.log(this.state, 'News state');
    }

    generateRequest = (query) => {
        const options = {
            apiUri: this.apiUri,
            query: `q=${query}`,
            from: moment().subtract(1, 'month').format('YYYY-MM-DD'),
            quantity: this.state.quantity
        };

        let uri = `${options.apiUri}` +
            `${options.query}&` +
            `pageSize=${options.quantity}&` +
            'language=en&' +
            'sortBy=publishedAt&' +
            `from=${options.from}&` +
            'apiKey=b49dbcbe459e42acb39bbbeb8b740dbe';

        return new Request(uri)
    }


    fetchData = async (req) => {
        this.setState({
            isLoaded: false
        });

        const res = await fetch(req);
        const data = await res.json();

        this.setState({
            isLoaded: true,
            news: data.articles
        });
    }

    toggleOpened = (key) => {
        this.setState({activeKey: this.state.activeKey === key ? null : key});
    };

    showMoreHandler = () => {
        this.setState({
                quantity: this.state.quantity + 10
            }
        )
    };

    render() {
        const {news, isLoaded} = this.state;

        let isGradientNeeded, showMoreBtn, Loading, renderNews;

        if (!isLoaded) Loading = (
            <SLoading style={{width: '3rem', height: '3rem'}} className='loading'>Loading...</SLoading>)

        if (isLoaded && (this.state.news ? this.state.news.length >= 10 : '')) {
            isGradientNeeded = (<div className='news-gradient'></div>)
        }

        if (isLoaded && (this.state.news ? this.state.news.length >= 10 : '')) {
            showMoreBtn = (<Button
                className='show-more'
                outline color="secondary"
                onClick={this.showMoreHandler}
            >
                Show more
            </Button>)
        }

        if (news && news !== 'undefined') {
            renderNews = news.map((article) => {
                const {author, content, description, publishedAt, source, title, url, urlToImage} = article;
                return (
                    this.state.activeKey === url ?
                        (
                            <div
                                className='news-item active'
                                key={url}
                                onClick={() => this.toggleOpened(url)}
                            >
                                <h3>{title}</h3>
                                <h5>
                                    <Badge>{author}</Badge>
                                </h5>
                                <div className='img-container'>
                                    <img src={`${urlToImage}`} alt=""/>
                                </div>

                                <p>{content}</p>

                                <Sp className="text-muted">{moment(publishedAt).format("dddd, MMMM Do YYYY, h:mm a")}</Sp>
                                <a className="alert-link" href={url}>{source.name}</a>
                            </div>
                        ) : (
                            <div
                                className='news-item'
                                key={url}
                                onClick={() => this.toggleOpened(url)}
                            >
                                <h3>{title}</h3>
                                <p>{moment(publishedAt).format("dddd, MMMM Do YYYY, h:mm a")}</p>
                            </div>
                        )
                )
            })
        }


        return (
            <div className='news-block'>

                {renderNews}

                {Loading}

                {showMoreBtn}

                {isGradientNeeded}

            </div>
        )
    }
}

News.protoTypes = {
    query: PropTypes.string
}

export default News;