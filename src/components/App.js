import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import '../styles/App.scss'
import moment from 'moment';
import News from "./News";
import {
    Container,
    Label,
    Input,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Collapse,
} from 'reactstrap';
import Bitcoin from "./Bitcoin";
import styled from 'styled-components';

const SLabel = styled(Label)`
  margin: 0;
  margin-right: 1em;
  
`;
const Sdiv = styled.div`
  display: flex;
  align-items: center;
`;

const mockData = [
    {
        "name": "Page A",
        "uv": 4000,
        "pv": 2400,
        "amt": 2400
    },
    {
        "name": "Page B",
        "uv": 3000,
        "pv": 1398,
        "amt": 2210
    },
    {
        "name": "Page C",
        "uv": 2000,
        "pv": 9800,
        "amt": 2290
    },
    {
        "name": "Page D",
        "uv": 2780,
        "pv": 3908,
        "amt": 2000
    },
    {
        "name": "Page E",
        "uv": 1890,
        "pv": 4800,
        "amt": 2181
    },
    {
        "name": "Page F",
        "uv": 2390,
        "pv": 3800,
        "amt": 2500
    },
    {
        "name": "Page G",
        "uv": 3490,
        "pv": 4300,
        "amt": 2100
    }
]

class App extends Component {
    constructor(props) {
        super(props);
        this.searchInput = React.createRef()
    }

    state = {
        query: 'ukraine',
        collapsed: true,
        isOpen: false,
        setIsOpen: false,
        apiUrl: ''
    };

    static getDerivedStateFromProps(props, state) {
        return state
    }

    initialState = {
    	apiUrl: this.setInitialState
    }

    static setInitialState(route) {
        switch (route) {
            case '/news':
        }
    }

    toggleNavbar = () => this.setState({
        collapsed: !this.state.collapsed
    })

    toggle = () => this.setState({
        setIsOpen: !this.state.isOpen
    })

    componentDidMount() {
        console.log(this.props);
        this.searchInput.current.focus();
        // this.fetchData('https://api.coindesk.com/v1/bpi/historical/close.json')
    }
    // componentDidUpdate(prevProps, prevState, snapshot) {
    //     if (prevProps.query !== this.props.query) {
    //         this.setState({
    //             quantity: 10
    //         });
    //         this.fetchData(req)
    //     } else if (prevState.quantity !== this.state.quantity) {
    //         this.fetchData(req)
    //     }
    // }

    inputHandler = (e) => {
        const query = e.target.value
        this.setState({
            query: query,
        });
    }

    fetchData = async (req) => {
        this.setState({
            isLoaded: false
        });

        const res = await fetch(req);
        const data = await res.json();

        const bitcoinData = data
        const sortedData = [];
        let count = 0;
        for (let date in bitcoinData.bpi){
            sortedData.push({
                date: moment(date).format('MMM DD'),
                priceDollar: bitcoinData.bpi[date].toFixed(2),
                // previousDate: count, //previous days
                pricePound: (bitcoinData.bpi[date]*0.75).toFixed(2) // numerical price
            });
            count++;
        }

        this.setState({
            isLoaded: true,
            bitcoinData: sortedData
        });
    }


    fetchBitcoinData = () => {
        const bitcoinData = data
        const sortedData = [];
        let count = 0;
        for (let date in bitcoinData.bpi){
            sortedData.push({
                d: moment(date).format('MMM DD'),
                p: bitcoinData.bpi[date].toLocaleString('us-EN',{ style: 'currency', currency: 'USD' }),
                x: count, //previous days
                y: bitcoinData.bpi[date] // numerical price
            });
            count++;
        }
        console.log(sortedData);
    }


    render() {
        console.log(location.pathname);
        return (
            <Router>
                <Container>
                    <div className='news-heading'>
                        <h1 className='glitch' data-text="SOME OTHER TIMES">SOME OTHER TIMES</h1>
                    </div>
                    <Navbar light expand="md">
                        <NavbarBrand href="#">
                            <Link to="/">SOMETIMES</Link>
                        </NavbarBrand>
                        <NavbarToggler onClick={this.toggleNavbar}/>
                        <Collapse isOpen={!this.state.collapsed} navbar>
                            <Nav className="mr-auto" navbar>
                                <NavItem>
                                    <NavLink>
                                        <Link to="/">NEWS</Link>
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink>
                                        <Link to="/bitcoin">BITCOIN</Link>
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink>
                                        <Link to="/weather">WEATHER</Link>
                                    </NavLink>
                                </NavItem>
                            </Nav>
                        </Collapse>

                    </Navbar>
                    <Sdiv>
                        <SLabel for="search-input">Search</SLabel>
                        <Input
                            id='search-input'
                            name='search'
                            type="text"
                            innerRef={this.searchInput}
                            placeholder='type here'
                            value={this.state.query ? this.state.query : ''}
                            onChange={this.inputHandler}
                        />
                    </Sdiv>
                    <Switch>
                        <Route
                            exact path='/bitcoin'
                            render={(props) => <Bitcoin
                                mockData = {mockData}
                                // locationHandler={this.locationHandler}
                                {...props}
                            />}
                        />
                        <Route
                            exact path='/'
                            render={(props) => <News {...props} query={this.state.query}/>}
                        />
                    </Switch>
                </Container>
            </Router>
        )
    }
}

export default App;