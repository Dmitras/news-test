import React, {useState, useEffect} from 'react';
import {Legend, Line, LineChart ,AreaChart, XAxis, YAxis, CartesianGrid, Tooltip, Area, ResponsiveContainer } from "recharts";
import Wrapper from "../hoc/Wrapper";


const Bitcoin = (props) => {
    const { data } = props;

    return (
        data.length ?
            <ResponsiveContainer width='100%' height={250}>
                <LineChart data={data}
                           margin={{top: 10, right: 0, left: 0, bottom: 0}}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="date" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Line type="monotone" dataKey="priceDollar" stroke="#8884d8" />
                    <Line type="monotone" dataKey="pricePound" stroke="#82ca9d" />
                </LineChart>
            </ResponsiveContainer > :
            null
    );
};

export default Wrapper(Bitcoin);